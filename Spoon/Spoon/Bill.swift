//
//  Bill.swift
//  Spoon
//
//  Created by Albert Villanueva on 2/9/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation

struct Bill {
	
	var products:[Product]?
	var totalPrice:Int
}
