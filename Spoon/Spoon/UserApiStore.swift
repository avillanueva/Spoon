//
//  UserApiStore.swift
//  Spoon
//
//  Created by Albert Villanueva on 11/7/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation

import Foundation
import FirebaseAuth
import Firebase
import FirebaseDatabase

class UserApiStore : UserStoreProtocol {
	
	func logInWithCredentials(request: LogInRequest, completionHandler: @escaping (LogInResponse) -> Void){
		
		if request.email != nil{
			self.logInWithEmailAndPassword(request: request, completionHandler: { (response) in
				completionHandler(response)
			})
		}else{
			self.logInWithTokenCredentials(request: request, completionHandler: { (response) in
				completionHandler(response)
			})
		}
	}
	
	func logInWithEmailAndPassword(request: LogInRequest, completionHandler: @escaping (LogInResponse) -> Void) {
		
		FIRAuth.auth()?.signIn(withEmail: request.email!, password: request.password!) { (user, error) in
			
			if((user) != nil){
				
				let newUser = User()
				newUser.userId = user?.uid
				newUser.email = user?.email
				newUser.name = user?.displayName
				newUser.photoUrl = user?.photoURL?.absoluteString
				
				completionHandler(LogInResponse(user: newUser, error: nil))
			}else if((error) != nil){
				
				completionHandler(LogInResponse(user: nil, error: ApiErrorManager.getApiError(error: error! as NSError)))
			}
		}
	}
	
	func logInWithTokenCredentials(request: LogInRequest, completionHandler: @escaping (LogInResponse) -> Void) {
		
		let credential = FIRGoogleAuthProvider.credential(withIDToken: request.idToken!, accessToken: request.accesToken!)
		
		FIRAuth.auth()?.signIn(with: credential) { (user, error) in
			
			if((user) != nil){
				
				let newUser = User()
				newUser.userId = user?.uid
				newUser.email = user?.email
				newUser.name = user?.displayName
				newUser.photoUrl = user?.photoURL?.absoluteString
				
				completionHandler(LogInResponse(user: newUser, error: nil))
			}else if((error) != nil){
				
				completionHandler(LogInResponse(user: nil, error: ApiErrorManager.getApiError(error: error! as NSError)))
			}
		}
	}
	
	func signUpUser(user: User!, completionHandler: @escaping (LogInResponse) -> Void) {
		
		let dataBase = FIRDatabase.database().reference()
		
		let userData = NSMutableDictionary()
		userData.setValue(user.name, forKey: "name")
		userData.setValue(user.email, forKey: "email")
//		if let photoUrl = user.photoUrl { userData.setValue(photoUrl, forKey: "photoUrl") }
		
		dataBase.child("users").child(user!.userId!).setValue(userData) { (error, dataBaseRef) in
			
			if error != nil{
				completionHandler(LogInResponse(user: nil, error: ApiErrorManager.getApiError(error: error! as NSError)))
			}else{
				completionHandler(LogInResponse(user: user, error: nil))
			}
		}
	}
	
	func getUser(user: User!, completionHandler: @escaping (User?, ApiError?) -> Void) {
		
		let dataBase = FIRDatabase.database().reference()
		
		dataBase.child("users").child(user!.userId!).observeSingleEvent(of: .value, with: { (snapshot) in
			
			let userDict = snapshot.value as? NSDictionary
			
			if let email = userDict?["email"] as? String {
				let savedUser = User()
				savedUser.userId = user.userId
				savedUser.email = email
				
				if let name = userDict?["name"]{
					savedUser.name = name as? String
				}
				if let photoUrl = userDict?["photoUrl"]{
					savedUser.photoUrl = photoUrl as? String
				}
				
				completionHandler(savedUser, nil)
			}else{
				completionHandler(nil, nil)
			}
			
			
			}) { (error) in
				print(error.localizedDescription)
				completionHandler(nil, ApiErrorManager.getApiError(error: error as NSError))
		}
	}
	
	internal func updateUser(user: User, completionHandler: @escaping (ApiError?) -> Void) {
		
	}
}
