//
//  RestaurantQRStore.swift
//  Spoon
//
//  Created by Albert Villanueva on 23/8/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation
import RealmSwift

class RestaurantQRStore: RestaurantStoreProtocol {
	
	func addRestaurant(restaurant: Restaurant, completionHandler:(_ response: ReadQRResponse) -> Void) {
		
		let realm = try! Realm()
		try! realm.write {
			realm.add(restaurant, update: true)
		}
		completionHandler(ReadQRResponse(error: nil, restaurant: restaurant))
	}
	
	class func getCurrentRestaurant() -> Restaurant? {
		
		let realm = try! Realm()
		let restaurants = realm.objects(Restaurant.self)
		return restaurants.first
	}
}
