//
//  MenuCategoryType.swift
//  Spoon
//
//  Created by Albert Villanueva on 20/7/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation

struct MenuProductType: Equatable {
	
	var name:String?
	var products:[MenuProductItem]?
}

func ==(lhs: MenuProductType, rhs: MenuProductType) -> Bool {
	
	return lhs.name == rhs.name
}