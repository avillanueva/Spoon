//
//  User.swift
//  Spoon
//
//  Created by Albert Villanueva on 11/7/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import RealmSwift

class User: Object{
	
	dynamic var userId:String?
	dynamic var email:String?
	dynamic var name:String?
	dynamic var photoUrl:String?
	
	override static func primaryKey() -> String? {
		return "userId"
	}
	
}
