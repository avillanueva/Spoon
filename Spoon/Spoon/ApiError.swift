//
//  ApiError.swift
//  Spoon
//
//  Created by Albert Villanueva on 7/9/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation
import FirebaseAuth

enum ApiError: String, Error {
	case NetWorkConnection
	case UserTokenExpired
	case TooManyRequests
	case InvalidAPIKey
	case AppNotAuthorized
	case KeychainError
	case InternalError
	case InvalidEmail
	case UserDisabled
	case WrongPassword
	case InvalidCredential
	case OperationNotAllowed
	case EmailAlreadyInUse
	case WeakPassword
	case CredentialAlreadyInUse
	case UserNotFound
	case UnknownError
	case CannotCreateRestaurant
}

class ApiErrorManager {
	
	class func getApiError(error: NSError) -> ApiError {
		
		switch error.code {
			
//		case FIRAuthErrorCode.errorCodeNetworkError:
//				return ApiError.NetWorkConnection
			
		case FIRAuthErrorCode.errorCodeUserTokenExpired.rawValue:
			return ApiError.UserTokenExpired
			
		case FIRAuthErrorCode.errorCodeTooManyRequests.rawValue:
			return ApiError.TooManyRequests
			
		case FIRAuthErrorCode.errorCodeInvalidAPIKey.rawValue:
			return ApiError.InvalidAPIKey
			
		case FIRAuthErrorCode.errorCodeAppNotAuthorized.rawValue:
			return ApiError.AppNotAuthorized
			
		case FIRAuthErrorCode.errorCodeKeychainError.rawValue:
			return ApiError.KeychainError
			
		case FIRAuthErrorCode.errorCodeInternalError.rawValue:
			return ApiError.InternalError
			
			case FIRAuthErrorCode.errorCodeInvalidEmail.rawValue:
			return ApiError.InvalidEmail
			
		case FIRAuthErrorCode.errorCodeUserDisabled.rawValue:
			return ApiError.UserDisabled
			
		case FIRAuthErrorCode.errorCodeWrongPassword.rawValue:
			return ApiError.WrongPassword
			
		case FIRAuthErrorCode.errorCodeWeakPassword.rawValue:
			return ApiError.WeakPassword
			
		case FIRAuthErrorCode.errorCodeCredentialAlreadyInUse.rawValue:
			return ApiError.CredentialAlreadyInUse
			
		case FIRAuthErrorCode.errorCodeUserNotFound.rawValue:
			return ApiError.UserNotFound
			
			default:
				return ApiError.UnknownError
		}
	}
}

	
