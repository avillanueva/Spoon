//
//  CategoryHeaderCollectionReusableView.swift
//  Spoon
//
//  Created by Albert Villanueva on 19/7/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import UIKit
import Kingfisher

class CategoryHeaderCollectionReusableView: UICollectionReusableView {
        
	@IBOutlet weak var restaurantImage: UIImageView!
	@IBOutlet weak var userNameLabel: UILabel!
	var restaurant:Restaurant!{
		
		didSet{
			
			if restaurant?.photoUrl != nil {
				let url = URL(string: restaurant.photoUrl!)
				let resource = ImageResource(downloadURL: url!)
				restaurantImage.kf_setImage(with: resource, placeholder: nil, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
			}
			if let name = restaurant?.name{
				self.userNameLabel.text = name
			}
		}
	}
}
