//
//  Restaurant.swift
//  Spoon
//
//  Created by Albert Villanueva on 19/7/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import RealmSwift

class Restaurant:Object {
	
	dynamic var restaurantId:String?
	dynamic var address:String?
	dynamic var name:String?
	dynamic var photoUrl:String?
	dynamic var tableId:String?
	override static func primaryKey() -> String? {
		return "restaurantId"
	}
	
	class func createRestaurantWithData(jsonData: [String:AnyObject]) -> Restaurant {
		
		let restaurant = Restaurant()
		
		if let restaurantId = jsonData["restaurant_id"]{
			restaurant.restaurantId = restaurantId as? String
		}
		if let name = jsonData["name"]{
			restaurant.name = name as? String
		}
		if let urlString = jsonData["logo"]{
			restaurant.photoUrl = urlString as? String
		}
		if let tableId = jsonData["taula"]{
			restaurant.tableId = tableId as? String
		}
		
		return restaurant
	}

}