//
//  ListBillConfigurator.swift
//  Spoon
//
//  Created by Albert Villanueva on 2/9/16.
//  Copyright (c) 2016 Albert Villanueva. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

import UIKit

// MARK: Connect View, Interactor, and Presenter

extension ListBillViewController: ListBillPresenterOutput
{
	func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
  {
    router.passDataToNextScene(segue: segue)
  }
}

extension ListBillInteractor: ListBillViewControllerOutput
{
}

extension ListBillPresenter: ListBillInteractorOutput
{
}

class ListBillConfigurator
{
  // MARK: Object lifecycle
	
	class var sharedInstance: ListBillConfigurator {
		struct Static {
			static let instance: ListBillConfigurator = ListBillConfigurator()
		}
		return Static.instance
	}
  
  // MARK: Configuration
  
  func configure(viewController: ListBillViewController)
  {
    let router = ListBillRouter()
    router.viewController = viewController
    
    let presenter = ListBillPresenter()
    presenter.output = viewController
    
    let interactor = ListBillInteractor()
    interactor.output = presenter
    
    viewController.output = interactor
    viewController.router = router
  }
}
