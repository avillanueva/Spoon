//
//  ProductMenuCell.swift
//  Spoon
//
//  Created by Albert Villanueva on 21/7/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import UIKit

class ProductMenuCell: UITableViewCell {

	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var productImageView: UIImageView!
	var productItem:ListProductMenuViewModel.MenuProductItemView?{
		didSet{
			nameLabel.text = productItem?.name
			self.selectionStyle = .none
		}
	}

}
