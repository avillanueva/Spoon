//
//  UserMemStore.swift
//  Spoon
//
//  Created by Albert Villanueva on 11/7/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation


class UserMemStore: UserStoreProtocol {
	
	func updateUser(user: User, completionHandler: @escaping (ApiError?) -> Void) {
		completionHandler(nil)
	}
	
	func logInWithCredentials(request: LogInRequest, completionHandler: @escaping (LogInResponse) -> Void) {
		
		completionHandler(LogInResponse(user: nil, error: nil))
	}
	
	func signUpUser(user: User!, completionHandler: @escaping (LogInResponse) -> Void) {
		
		completionHandler(LogInResponse(user: nil, error: nil))
	}
	
	func getUser(user: User!, completionHandler: @escaping (User?, ApiError?) -> Void){
		
		completionHandler(nil, nil)
	}
}
