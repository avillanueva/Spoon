//
//  SessionManager.swift
//  Spoon
//
//  Created by Albert Villanueva on 27/9/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation
import RealmSwift

class SessionManager {
	
	class func checkIfUserIsLoggedIn(){
		
		guard let _ = UserRealmStore.getCurrentUser() else{
			
			logOut()
			goToLogin()
			return
		}
	}
	
	class func logOut() {
		
		removeSensitiveData()
	}
	
	private class func goToLogin() {
		
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let loginStroyBoard = UIStoryboard(name: "Login", bundle: nil)
		let loginVC = loginStroyBoard.instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
		appDelegate.window?.rootViewController  = loginVC
	}
	
	private class func removeSensitiveData() {
		
		removePersistenceData()
	}
	
	private class func removePersistenceData(){
		
		let realm = try! Realm()
		try! realm.write {
			realm.deleteAll()
		}
	}
}
