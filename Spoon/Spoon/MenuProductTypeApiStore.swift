//
//  MenuProductTypeApiStore.swift
//  Spoon
//
//  Created by Albert Villanueva on 20/7/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import RealmSwift

class MenuProductTypeApiStore: MenuProductTypeStore {
	
	func getMenuProductTypes(request: ListProductMenuRequest, completionHandler: @escaping (_ response: ListProductMenuResponse) -> Void) {
		
		let dataBase = FIRDatabase.database().reference()
		
		let currentRestaurant = RestaurantQRStore.getCurrentRestaurant()
		
		dataBase.child("products").child(currentRestaurant!.restaurantId!).queryOrdered(byChild: "category").queryEqual(toValue: request.category?.categoryId!).observe(.value, with: { (snapshot) in
			
			if let snapshotData = snapshot.children.allObjects as? [FIRDataSnapshot]{
				
				for child in snapshotData{
					
					let childDict = child.value as! NSDictionary
					
					var product = Product()
					
					product.name = childDict["name"] as? String
					product.price = childDict["price"] as! Int
					product.category = childDict["category"] as? String
					product.productId = childDict["product_id"] as? String
					let items = childDict["items"] as? NSArray
					
					var menuProductTypes: [MenuProductType] = []

					for typeData in items! {
						
						for typeArray in typeData as! NSDictionary{
							
							var menuType = MenuProductType()
							
							menuType.name = typeArray.key as? String
							let menuItems = typeArray.value as? NSArray
							
							var menuProductItems: [MenuProductItem] = []
							for item in menuItems!{
								
								let menuItem = MenuProductItem(name: item as? String, imageUrl: nil, quantity: nil, selected: false)
								
								menuProductItems.append(menuItem)
							}
							
							menuType.products = menuProductItems
							menuProductTypes.append(menuType)
						}
						product.items = menuProductTypes
						
					}
					completionHandler(ListProductMenuResponse(product: product, error: nil))
				}
			}

			}) { (error) in
				completionHandler(ListProductMenuResponse(product: nil, error: ApiErrorManager.getApiError(error: error as NSError)))
		}
	}
}
