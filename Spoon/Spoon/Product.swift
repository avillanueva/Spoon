//
//  MenuProductItem.swift
//  Spoon
//
//  Created by Albert Villanueva on 20/7/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation

struct Product: Equatable {
	var name:String?
	var productId:String?
	var imageUrl:NSURL?
	var price:Int = 0
	var quantity:Int = 1
	var category:String?
	var items:[MenuProductType]?
}

func ==(lhs: Product, rhs: Product) -> Bool {
	
	return lhs.name == rhs.name && lhs.productId == rhs.productId && lhs.quantity == rhs.quantity
}


struct ProductViewModel {
	
	var name:String = ""
	var productId:String = ""
	var image:UIImage?
	var price:String = ""
	var quantity:Int = 0
	var hasItems:Bool?
}
