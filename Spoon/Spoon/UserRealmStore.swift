//
//  UserRealmStore.swift
//  Spoon
//
//  Created by Albert Villanueva on 23/8/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation
import RealmSwift

class UserRealmStore: UserStoreProtocol {

	func updateUser(user: User, completionHandler: @escaping (ApiError?) -> Void) {
		
		let realm = try! Realm()
		try! realm.write {
			realm.add(user, update: true)
		}
		completionHandler(nil)
	}
	
	func getUser(user: User!, completionHandler: @escaping (User?, ApiError?) -> Void) {
		
		let realm = try! Realm()
		let users = realm.objects(User.self)
		if let savedUser = users.first {
			completionHandler(savedUser, nil)
		}else{
			completionHandler(nil, ApiError.UserNotFound)
		}

	}
	
	class func getCurrentUser() -> User? {
		
		let realm = try! Realm()
		let users = realm.objects(User.self)
		return users.first
	}
	
	func signUpUser(user: User!, completionHandler: @escaping (LogInResponse) -> Void) {}
	func logInWithCredentials(request: LogInRequest, completionHandler: @escaping (LogInResponse) -> Void) {}
	
}
