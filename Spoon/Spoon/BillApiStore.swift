//
//  BillApiStore.swift
//  Spoon
//
//  Created by Albert Villanueva on 2/9/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class BillApiStore: BillStoreProtocol {
	
	func getBill(request: ListBillRequest, completionHandler: @escaping (_ response: ListBillResponse) -> Void) {
		
		let dataBase = FIRDatabase.database().reference()
		
		let currentRestaurant = RestaurantQRStore.getCurrentRestaurant()
		
		dataBase.child("orders").child(currentRestaurant!.restaurantId!).child(currentRestaurant!.tableId!).queryOrdered(byChild: "open").queryEqual(toValue: true).observeSingleEvent(of: .value, with: { (snapshot) in
			
			var products:[Product] = []
			var total = 0
			if let snapshotData = snapshot.children.allObjects as? [FIRDataSnapshot]{
				
				for child in snapshotData{
					
					let childDict = child.value as! NSDictionary
					
					let orderItems = childDict["order_items"] as? NSArray
					
					for productData in orderItems!{
						
						let productDataDict = productData as! NSDictionary
						
						var product = Product()
						
						product.name = productDataDict["name"] as? String
						product.price = productDataDict["price"] as! Int
						product.quantity = productDataDict["quantity"] as! Int
						products.append(product)
						
					}
					let orderPrice = childDict["total"] as! Int
					total += orderPrice
				}
			}

			completionHandler(ListBillResponse(bill: Bill(products: products, totalPrice: total), error: nil))
			
			}) { (error) in
				
				//FIXME: SHOULD CONTROL THE KIND OF ERROR THROWN TO PROPAGATE WITH CORRECT ERROR DESCRITPTION
				
				completionHandler(ListBillResponse(bill: nil, error: ApiErrorManager.getApiError(error: error as NSError)))
		}

	}
	
	func updateBill(request: ListBillRequest, completionHandler: @escaping (_ response: ListBillResponse) -> Void) {
		
		let dataBase = FIRDatabase.database().reference()
		
		let currentRestaurant = RestaurantQRStore.getCurrentRestaurant()
		dataBase.child("orders").child(currentRestaurant!.restaurantId!).child(currentRestaurant!.tableId!).queryOrdered(byChild: "open").queryEqual(toValue: true).observeSingleEvent(of: .value, with: { (snapshot) in
			
			if let snapshotData = snapshot.children.allObjects as? [FIRDataSnapshot]{
				
				for child in snapshotData{
					
					let childUpdates = ["/orders/\(currentRestaurant!.restaurantId!)/\(currentRestaurant!.tableId!)/\(child.key)/open": false]
					dataBase.updateChildValues(childUpdates)
				}
			}
			
			completionHandler(ListBillResponse(bill: nil, error: nil))
			
			}, withCancel: { (error) in
				
			completionHandler(ListBillResponse(bill: nil, error: ApiErrorManager.getApiError(error: error as NSError)))
		})
		
	}
}
