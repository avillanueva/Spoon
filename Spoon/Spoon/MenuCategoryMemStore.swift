//
//  MenuCategoryMemStore.swift
//  Spoon
//
//  Created by Albert Villanueva on 18/7/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation


class MenuCategoryMemStore: MenuCategoryStore {
	
	func getMenuCategories(request: ListMenusRequest, completionhandler:@escaping (_ response: ListMenusResponse) -> Void) {
		completionhandler(ListMenusResponse())
	}
}
