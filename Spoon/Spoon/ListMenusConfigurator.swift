//
//  ListMenusConfigurator.swift
//  Spoon
//
//  Created by Albert Villanueva on 18/7/16.
//  Copyright (c) 2016 Albert Villanueva. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

import UIKit

// MARK: Connect View, Interactor, and Presenter

extension ListMenusViewController: ListMenusPresenterOutput
{

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    self.router.passDataToNextScene(segue: segue)
  }
}

extension ListMenusInteractor: ListMenusViewControllerOutput
{
}

extension ListMenusPresenter: ListMenusInteractorOutput
{
}

class ListMenusConfigurator
{
  // MARK: Object lifecycle
	
	class var sharedInstance: ListMenusConfigurator {
		struct Static {
			static let instance: ListMenusConfigurator = ListMenusConfigurator()
		}
		return Static.instance
	}
  
  // MARK: Configuration
  
  func configure(viewController: ListMenusViewController)
  {
    let router = ListMenusRouter()
    router.viewController = viewController
    
    let presenter = ListMenusPresenter()
    presenter.output = viewController
    
    let interactor = ListMenusInteractor()
    interactor.output = presenter
    
    viewController.output = interactor
    viewController.router = router
  }
}
