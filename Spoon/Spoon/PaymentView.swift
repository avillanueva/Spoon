//
//  PaymentView.swift
//  Spoon
//
//  Created by Albert Villanueva on 6/9/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import UIKit

protocol PaymentViewDelegate: class {
	func didPressedCashButton()
}

class PaymentView: UIView {

	var contentView = UIView()
	var nibName = "PaymentView"
	weak var delegate:PaymentViewDelegate?
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setup(frame: frame)
	}
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup(frame: nil)
	}
	func setup(frame:CGRect?) {
		
		contentView = loadViewFromNib()
		contentView.frame = CGRect(x: frame!.width / 2 - 180, y:frame!.height * 2, width: 360, height: 250)
		contentView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
		contentView.layer.masksToBounds = false;
		contentView.layer.shadowOffset = CGSize(width: 0, height: 5)
		contentView.layer.shadowRadius = 5
		contentView.layer.shadowOpacity = 0.5
		contentView.layer.cornerRadius = 20
		addSubview(contentView)
		
		let closeGesture = UITapGestureRecognizer(target: self, action: #selector(animateViewTransition(appearing:)))
		self.addGestureRecognizer(closeGesture)
	}
	
	func loadViewFromNib() -> UIView {

		let bundle = Bundle(for: type(of: self))
		let nib = UINib(nibName: nibName, bundle: bundle)
		let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
		
		return view!
	}
	
	func showView() {
		animateViewTransition(appearing: true)
	}

	@IBAction func cashButtonPressed(_ sender: UIButton) {
		
		animateViewTransition(appearing: false)
		delegate?.didPressedCashButton()
	}
	
	func animateViewTransition(appearing: Bool) {

		UIView.animate(withDuration: 0.7, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: .curveEaseIn, animations: {
			
			self.backgroundColor = appearing ? UIColor.init(red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 0.5) : UIColor.clear
			self.contentView.center = appearing ? CGPoint(x: self.center.x, y: self.center.y) : CGPoint(x: self.frame.size.width / 2, y: self.frame.size.height * 2)
			
			}, completion: { _ in
				if appearing == false { self.removeFromSuperview()}
		})
	}

}
