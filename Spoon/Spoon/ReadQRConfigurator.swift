//
//  ReadQRConfigurator.swift
//  Spoon
//
//  Created by Albert Villanueva on 13/7/16.
//  Copyright (c) 2016 Albert Villanueva. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

import UIKit

// MARK: Connect View, Interactor, and Presenter

extension ReadQRViewController: ReadQRPresenterOutput
{
	func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
  {
    router.passDataToNextScene(segue: segue)
  }
}

extension ReadQRInteractor: ReadQRViewControllerOutput
{
}

extension ReadQRPresenter: ReadQRInteractorOutput
{
}

class ReadQRConfigurator
{
  // MARK: Object lifecycle
	
	class var sharedInstance: ReadQRConfigurator {
		struct Static {
			static let instance: ReadQRConfigurator = ReadQRConfigurator()
		}
		return Static.instance
	}
  
  // MARK: Configuration
  
  func configure(viewController: ReadQRViewController)
  {
    let router = ReadQRRouter()
    router.viewController = viewController
    
    let presenter = ReadQRPresenter()
    presenter.output = viewController
    
    let interactor = ReadQRInteractor()
    interactor.output = presenter
    
    viewController.output = interactor
    viewController.router = router
  }
}
