//
//  MenuProductTypeMemStore.swift
//  Spoon
//
//  Created by Albert Villanueva on 20/7/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation

class MenuProductTypeMemStore: MenuProductTypeStore {
	
	func getMenuProductTypes(request: ListProductMenuRequest, completionHandler:@escaping (_ response: ListProductMenuResponse) -> Void) {
		completionHandler(ListProductMenuResponse(product: nil, error: nil))
	}
}

