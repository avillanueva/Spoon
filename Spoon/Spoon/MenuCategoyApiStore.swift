//
//  MenuCategoyApiStore.swift
//  Spoon
//
//  Created by Albert Villanueva on 18/7/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase


class MenuCategoryApiStore: MenuCategoryStore {
	
	func getMenuCategories(request: ListMenusRequest, completionhandler: @escaping (_ response: ListMenusResponse) -> Void) {
		
		let dataBase = FIRDatabase.database().reference()
	
		dataBase.child("categories").child((request.restaurant?.restaurantId)!).observe(.value, with: { (snapshot) in
			
			var categories:[MenuCategory] = []
			if snapshot.hasChildren(){
				categories.removeAll()
				for child in snapshot.value as! NSDictionary{
					var category = MenuCategory()
					category.categoryId = child.key as? String
					let childValue = child.value as! NSDictionary
					category.name = childValue.object(forKey: "name") as? String
					categories.append(category)
				}
			}
			let response = ListMenusResponse(categories:categories, error: nil)
			
			completionhandler(response)
			
			}) { (error) in
				completionhandler(ListMenusResponse(categories: nil, error: MenuCategoryApiError.UnknownError))
		}

	}
}

enum MenuCategoryApiError: String, Error {
	case UnknownError = "Unknown error"
}
