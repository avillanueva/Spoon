//
//  MenuCategory.swift
//  Spoon
//
//  Created by Albert Villanueva on 18/7/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation

struct MenuCategory: Equatable {
	
	var name:String?
	var categoryId:String?
	
}

func ==(lhs: MenuCategory, rhs: MenuCategory) -> Bool{
	
	return lhs.categoryId == rhs.categoryId && lhs.name == rhs.name
	
}