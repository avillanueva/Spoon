//
//  BillTableViewCell.swift
//  Spoon
//
//  Created by Albert Villanueva on 2/9/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import UIKit

class BillTableViewCell: UITableViewCell {

	@IBOutlet weak var quantityLabel: UILabel!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var priceLabel: UILabel!
	
	var product: ProductViewModel?{
		didSet{
			
			if let name = product?.name{
				nameLabel.text = name
			}
			if let price = product?.price {
				priceLabel.text = price
			}
			quantityLabel.text = "\(product!.quantity)"
		
		}
	}
}
