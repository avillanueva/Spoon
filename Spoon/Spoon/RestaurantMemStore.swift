//
//  RestaurantMemStore.swift
//  Spoon
//
//  Created by Albert Villanueva on 25/8/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation

class RestaurantMemStore: RestaurantStoreProtocol {
	
	func addRestaurant(restaurant: Restaurant, completionHandler: (_ response: ReadQRResponse) -> Void) {
		
		completionHandler(ReadQRResponse(error: nil, restaurant: restaurant))
	}
}
