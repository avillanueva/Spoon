//
//  ReadQRInteractor.swift
//  Spoon
//
//  Created by Albert Villanueva on 13/7/16.
//  Copyright (c) 2016 Albert Villanueva. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

import UIKit

protocol ReadQRInteractorInput
{
  func requestQRValidation(request:ReadQRRequest)
}

protocol ReadQRInteractorOutput
{
  func presentQRValidationResponse(response: ReadQRResponse)
}

class ReadQRInteractor: ReadQRInteractorInput
{
  var output: ReadQRInteractorOutput!
  var persistenceWorker: ReadQRWorker! = ReadQRWorker(store: RestaurantQRStore())
	
  
  // MARK: Business logic
  
	func requestQRValidation(request: ReadQRRequest) {
		
		if request.QRText != nil{
			createJsonFromRequest(request: request)
		}else{
			self.output.presentQRValidationResponse(response: ReadQRResponse(error: ApiError.CannotCreateRestaurant, restaurant: nil))
		}
	}
	
	func createJsonFromRequest(request: ReadQRRequest) {
		
		do {
			let json = try persistenceWorker.createJsonFromQRString(request: request)
			let restaurant = Restaurant.createRestaurantWithData(jsonData: json!)
			
			addRestaurant(restaurant: restaurant)
		}
		catch {
			self.output.presentQRValidationResponse(response: ReadQRResponse(error: ApiError.CannotCreateRestaurant, restaurant: nil))
		}
	}
	
	func addRestaurant(restaurant: Restaurant) {
		
		persistenceWorker.addRestaurant(restaurant: restaurant, completionHandler: { (response) in
		self.output.presentQRValidationResponse(response: response!)
		})
	}

}
