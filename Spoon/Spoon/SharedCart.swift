//
//  SharedCart.swift
//  Spoon
//
//  Created by Albert Villanueva on 9/8/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation

class SharedCart {
	static let sharedInstance = SharedCart()
	private init() {cartProducts = []}
	
	var cartProducts:[Product]?
	var selectedProductsCounter = 0
	
	func addProduct(product:Product) {
		
		cartProducts?.append(product)
		selectedProductsCounter += 1
	}
	
	func removeAllProducts() {
		
		cartProducts?.removeAll()
		selectedProductsCounter = 0
	}
}
