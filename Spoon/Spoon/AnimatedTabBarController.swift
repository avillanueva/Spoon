//
//  AnimatedTabBarController.swift
//  Spoon
//
//  Created by Albert Villanueva on 28/9/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import UIKit

class AnimatedTabBarController: UITabBarController, UITabBarControllerDelegate {
	
	var animator:TabBarAnimator!
	
	override func viewDidLoad() {
			super.viewDidLoad()
		animator = TabBarAnimator(tabBarController: self)
		self.delegate = self
	}

	//MARK: TabBarControllerDelegate
	
	func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning?{
		
		animator.fromVC = fromVC
		animator.toVC = toVC

		if (tabBarController.viewControllers?.index(of: fromVC))! < (tabBarController.viewControllers?.index(of: toVC))!{
			animator.movingRight = true
		}else{
			animator.movingRight = false
		}
		return animator

	}
}


class TabBarAnimator: UIPercentDrivenInteractiveTransition, UIViewControllerAnimatedTransitioning, UIGestureRecognizerDelegate {
	
	let animationDuration = 0.6
	var movingRight = true
	
	var fromVC:UIViewController?
	var toVC:UIViewController?
	var tabBarController:UITabBarController?
	var transitionContext:UIViewControllerContextTransitioning?
	
	init(tabBarController: AnimatedTabBarController) {
		super.init()
		self.tabBarController = tabBarController
	}
	
	func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
		
		return animationDuration
	}
	
	
	func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
		
		let containerView = transitionContext.containerView
		let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from)!
		let toView = transitionContext.view(forKey: UITransitionContextViewKey.to)!

		containerView.addSubview(toView)
		containerView.addSubview(fromView)
		
		let offScreenRight = CGAffineTransform(translationX: containerView.frame.width, y: 0)
		let offScreenLeft = CGAffineTransform(translationX: -containerView.frame.width, y: 0)
		
		containerView.bringSubview(toFront: fromView)
		
		toVC?.view.transform = self.movingRight ? offScreenRight : offScreenLeft
		
		UIView.animate(withDuration: animationDuration, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.0, options: .curveEaseIn, animations: {
			
			self.fromVC?.view.transform  = self.movingRight ? offScreenLeft : offScreenRight
			
			self.toVC?.view.transform = CGAffineTransform.identity
			
			}, completion: { _ in
				containerView.bringSubview(toFront: toView)
				self.fromVC?.view.transform = CGAffineTransform.identity
				transitionContext.completeTransition(true)
		})
	}
}
