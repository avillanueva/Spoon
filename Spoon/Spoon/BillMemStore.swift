//
//  BillMemStore.swift
//  Spoon
//
//  Created by Albert Villanueva on 2/9/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation

class BillMemStore: BillStoreProtocol {
	
	func getBill(request: ListBillRequest, completionHandler:@escaping (_ response: ListBillResponse) -> Void) {
		completionHandler(ListBillResponse(bill: nil, error: nil))
	}
	
	func updateBill(request: ListBillRequest, completionHandler:@escaping (_ response: ListBillResponse) -> Void) {
		
		completionHandler(ListBillResponse(bill: nil, error: nil))
	}
}
