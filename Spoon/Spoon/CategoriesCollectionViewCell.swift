//
//  CategoriesCollectionViewCell.swift
//  Spoon
//
//  Created by Albert Villanueva on 19/7/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {
	
	
	@IBOutlet weak var categoryImageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	
	func getImage() {
		
		if nameLabel.text != nil{
			
			switch nameLabel.text! {
			case "Carta":
				categoryImageView.image = UIImage(named: "category_item_carta")
			case "Beguda":
				categoryImageView.image = UIImage(named: "category_item_begudes")
			case "Menú":
				categoryImageView.image = UIImage(named: "category_item_menu")
			case "Postres":
				categoryImageView.image = UIImage(named: "category_item_postres")
			case "Tapes":
				categoryImageView.image = UIImage(named: "category_item_tapes")
			default:
				break
			}
		}
	}

	override func awakeFromNib() {
		layer.borderWidth = 1.0
		layer.borderColor = UIColor.lightGray.cgColor
	}
	
}
