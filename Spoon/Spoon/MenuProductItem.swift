//
//  MenuProductItem.swift
//  Spoon
//
//  Created by Albert Villanueva on 26/8/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation

struct MenuProductItem: Equatable {
	
	var name:String?
	var imageUrl:NSURL?
	var quantity:Int?
	var selected:Bool = false
}

func ==(lhs: MenuProductItem, rhs: MenuProductItem) -> Bool {
	
	return lhs.name == rhs.name && lhs.imageUrl == rhs.imageUrl
}