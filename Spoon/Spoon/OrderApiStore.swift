//
//  OrderApiStore.swift
//  Spoon
//
//  Created by Albert Villanueva on 1/8/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class OrderApiStore: OrderStore {
	
	func sendNewOrder(request: ListShoppingCartRequest, completionHandler: @escaping (_ response: ListShoppingCartResponse) -> Void) {
		

		if request.selectedProducts != nil{
		
			let currentRestaurant = RestaurantQRStore.getCurrentRestaurant()
			
			let dataBase = FIRDatabase.database().reference()
			
			var products:[NSMutableDictionary] = []
			
			var total: Int = 0
			for product in request.selectedProducts!{
				
				let productData:NSMutableDictionary = NSMutableDictionary()
				productData.setValue(product.quantity, forKey: "quantity")
				productData.setValue(product.name, forKey: "name")
				productData.setValue(product.price, forKey: "price")
				
				if product.items!.count > 0 {
					
					var items:[NSMutableDictionary] = []
					for menuType in product.items! {
						let types = NSMutableDictionary()
						var menuItems:[String]  = []
						for item in menuType.products! {
							
							if item.selected{
								menuItems.append(item.name!)
							}
						}
						types.setObject(menuItems, forKey: menuType.name! as NSCopying)
						items.append(types)
					}
					
					productData.setObject(items, forKey: "items" as NSCopying)
			
				}
				products.append(productData)
				
				
				total += product.price
			}
			
			let orderData:NSMutableDictionary = NSMutableDictionary()
			orderData.setValue(true, forKey: "open")
			orderData.setObject(products, forKey: "order_items" as NSCopying)
			orderData.setValue(total, forKey: "total")
			dataBase.child("orders").child((currentRestaurant?.restaurantId)!).child((currentRestaurant?.tableId)!).childByAutoId().setValue(orderData, withCompletionBlock: { (error, snapshot) in
				
				if (error != nil){
					completionHandler(ListShoppingCartResponse(error: OrderApiStoreError.UnknownError))
				}else{
					completionHandler(ListShoppingCartResponse(error: nil))
				}
			})
			
		}else{
			completionHandler(ListShoppingCartResponse(error: OrderApiStoreError.UnknownError))
		}
	}
	
	
}

enum OrderApiStoreError: String, Error {
	case UnknownError = "Unknown error"
}
