//
//  OrderMemStore.swift
//  Spoon
//
//  Created by Albert Villanueva on 1/8/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import Foundation

class OrderMemStore: OrderStore {
	
	func sendNewOrder(request: ListShoppingCartRequest, completionHandler:@escaping (_ response: ListShoppingCartResponse) -> Void) {
		completionHandler(ListShoppingCartResponse(error:nil))
	}
}
