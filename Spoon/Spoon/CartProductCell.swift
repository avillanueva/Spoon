//
//  CartProductCell.swift
//  Spoon
//
//  Created by Albert Villanueva on 2/8/16.
//  Copyright © 2016 Albert Villanueva. All rights reserved.
//

import UIKit

protocol CartProductCellDelegate: class {
	func didChangeProductQttyValue(sender: CartProductCell, increment:Bool)
}

class CartProductCell: UITableViewCell {
	
	weak var delegate:CartProductCellDelegate?
	var product:ProductViewModel?{
		didSet{
			if let name = product?.name {
				nameLabel.text = name
			}
			qttyLabel.text = "\(product!.quantity)"

			qttyLabel.isHidden = product!.hasItems! ? true : false
			increaseQttyButton.isHidden = product!.hasItems! ? true : false
			decreaseQttyButton.isHidden = product!.hasItems! ? true : false
		}
	}
	
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var qttyLabel: UILabel!
	@IBOutlet weak var decreaseQttyButton: UIButton!
	@IBOutlet weak var increaseQttyButton: UIButton!
	
	@IBAction func changeQttyButtonsChanged(sender: UIButton) {
		
		if sender.titleLabel?.text == "+" {
			product?.quantity += 1
			delegate?.didChangeProductQttyValue(sender: self, increment: true)
		}else if qttyLabel.text != "0" {
			product?.quantity -= 1
			delegate?.didChangeProductQttyValue(sender: self, increment: false)
		}
	}
}
