//
//  ListMenusViewControllerTests.swift
//  Spoon
//
//  Created by Albert Villanueva on 18/7/16.
//  Copyright (c) 2016 Albert Villanueva. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

@testable import Spoon
import XCTest

class ListMenusViewControllerTests: XCTestCase
{
  // MARK: Subject under test
  
  var sut: ListMenusViewController!
  var window: UIWindow!
	
  
  // MARK: Test lifecycle
  
  override func setUp()
  {
    super.setUp()
    window = UIWindow()
    setupListMenusViewController()
  }
  
  override func tearDown()
  {
    window = nil
    super.tearDown()
  }
  
  // MARK: Test setup
  
  func setupListMenusViewController()
  {
    let bundle = NSBundle.mainBundle()
    let storyboard = UIStoryboard(name: "Main", bundle: bundle)
    sut = storyboard.instantiateViewControllerWithIdentifier("ListMenusViewController") as! ListMenusViewController
  }
	
  func loadView()
  {
    window.addSubview(sut.view)
    NSRunLoop.currentRunLoop().runUntilDate(NSDate())
  }
  
  // MARK: Test doubles
	class ListMenusViewControllerOutputSpy: ListMenusViewControllerOutput {
		
		//method call expectations
		var fetchMenusCalled = false
		
		func fetchMenus(request:ListMenusRequest) {
			fetchMenusCalled = true
		}
	}
  
  // MARK: Tests
  
  func testFetchMenusOnLoadCallsFetchMenusFromInteractor()
  {
    // Given
		let listmMenusViewControllerOutputSpy = ListMenusViewControllerOutputSpy()
		sut.output = listmMenusViewControllerOutputSpy
    
    // When
		loadView()
		
    // Then
		XCTAssert(listmMenusViewControllerOutputSpy.fetchMenusCalled, "Should call to fetch menus when view is loaded")
  }

}
