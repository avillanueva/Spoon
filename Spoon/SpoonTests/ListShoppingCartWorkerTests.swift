//
//  ListShoppingCartWorkerTests.swift
//  Spoon
//
//  Created by Albert Villanueva on 1/8/16.
//  Copyright (c) 2016 Albert Villanueva. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

@testable import Spoon
import XCTest

class ListShoppingCartWorkerTests: XCTestCase
{
  // MARK: Subject under test
  
  var sut: ListShoppingCartWorker!
  
  // MARK: Test lifecycle
  
  override func setUp()
  {
    super.setUp()
    setupListShoppingCartWorker()
  }
  
  override func tearDown()
  {
    super.tearDown()
  }
  
  // MARK: Test setup
  
  func setupListShoppingCartWorker()
  {
    sut = ListShoppingCartWorker(store: OrderMemStoreSpy())
  }
  
  // MARK: Test doubles
	class OrderMemStoreSpy: OrderStore {
		
		var sendNewOrderCalled = false
		
		func sendNewOrder(request: ListShoppingCartRequest, completionHandler: (response: ListShoppingCartResponse) -> Void) {
			
			sendNewOrderCalled = true
			
			let oneSecond = dispatch_time(dispatch_time_t(DISPATCH_TIME_NOW), 1 * Int64(NSEC_PER_SEC))
			
			dispatch_after(oneSecond, dispatch_get_main_queue(), {
				completionHandler(response: ListShoppingCartResponse(error: nil))
			})
		}
	}

	
  // MARK: Tests
	func testShouldAskOrderStoreToSendNewOrderAndReturnResponse() {
		
		//Given
		let orderMemStoreSpy = sut.store as! OrderMemStoreSpy
		
		//When 
		let expectation = expectationWithDescription("Wait for response")
		let request = ListShoppingCartRequest(selectedProducts: [Seeds.dailyMeal])
		sut.sendNewOrder(request) { (response) in
			expectation.fulfill()
		}
		
		//Then
		XCTAssert(orderMemStoreSpy.sendNewOrderCalled, "Calling sendNewOrder should call store")
		waitForExpectationsWithTimeout(1.1) { (error) in
			
			XCTAssert(true, "should call completion handler when request completes")
		}
		
	}
	
	
}
